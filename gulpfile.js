/* eslint-disable */

/*
 * Modules
======================================== */
const gulp = require("gulp"); // gulpを動かす
const path = require("path"); // 安全にパスを解決する
const settings = require(path.join(__dirname, "settings.json")); // 初期設定はsettings.jsonにまとめる
const $ = require("gulp-load-plugins")(); // gulp-*プラグインをまとめて読み込む
const gifsicle = require("imagemin-gifsicle"); // imageminのプラグイン .gif
const jpegRecompress = require("imagemin-jpeg-recompress"); // imageminのプラグイン .jpg
const pngquant = require("imagemin-pngquant"); // imageminのプラグイン .png
const svgo = require("imagemin-svgo"); // imageminのプラグイン .svg
const fs = require("fs"); // ファイルを操作する
const cpx = require("cpx"); // ファイルコピー
const del = require("del"); // ファイル削除
const browserSync = require("browser-sync").create(); // ブラウザと同期する
const through = require("through2"); // through2オブジェクトを使用する

/*
 * Tasks
======================================== */

gulp.task("mkdir", mkdir); // ディレクトリを掘る
gulp.task("clean", clean); // ビルド前に掃除する
gulp.task("clone", clone); // vendor等を公開領域に転送する
gulp.task("ejs", ejs); // ejsをビルドする
gulp.task("imagemin", imagemin); // 画像を圧縮して公開領域に転送する
gulp.task("sprite", sprite); // スプライト画像を作成する
gulp.task("server", server); // サーバーを立ててブラウザと同期する

// ビルド系
gulp.task("build", gulp.series("imagemin", "sprite", "ejs"));

// ウォッチ系
gulp.task("watch", done => {
  // ejsとpageconfig
  gulp
    .watch([`${settings.ejs.src}**/*`, settings.ejs.pageconfig])
    .on("all", gulp.series("ejs"));
  // img最適化 src -> dest
  gulp.watch([`${settings.img.src}**/*.*`]).on("all", gulp.series("imagemin"));
  // vendorの転送 src -> dest
  settings.vendor.forEach(item => {
    gulp.watch(`${item.src}**/*.*`).on("all", gulp.series("clone"));
  });
  // スプライト画像
  gulp.watch([`${settings.sprite.src}*.svg`]).on("all", gulp.series("sprite"));
  done();
});

// ウォッチ系
gulp.task("default", gulp.parallel("server", "watch"));

/*
 * Functions
======================================== */

/**
 * settings.jsonのdirを元に空のディレクトリを作る
 */
function mkdir(done) {
  settings.dir.forEach(path => {
    gulp.src("*.*", { read: false }).pipe(gulp.dest(path));
  });
  done();
}

/**
 * ビルド前の掃除
 */
function clean(done) {
  del(
    [`${settings.basedir}**/*.html`, settings.js.dest, settings.css.dest],
    done()
  );
}

/**
 * vendor等のコピー
 */
function clone(done) {
  settings.vendor.forEach(item => {
    gulp
      .src(`${item.src}**/*`, { since: gulp.lastRun(done) })
      .pipe(gulp.dest(`${item.dest}`));
  });
  gulp
    .src(
      settings.ejs.cloneExt.map(item => {
        return `${settings.ejs.src}/**/*\.${item}`;
      }),
      { since: gulp.lastRun(done) }
    )
    .pipe(gulp.dest(settings.ejs.dest));
  done();
}

/**
 * ejs,html,phpをコンパイルして公開領域へ
 */
function ejs(done) {
  const pageconfig = JSON.parse(
    fs.readFileSync(path.join(__dirname, settings.ejs.pageconfig))
  );

  gulp
    .src([`${settings.ejs.src}**/!(_)*.ejs`])
    .pipe($.plumber({ errorHandler: notifyError() }))
    .pipe(
      $.data(file => {
        const currentDir = file.path.replace(/(\/|\\)[^(\/|\\)]*.ejs$/, ""); // ファイルのディレクトリのみ切り出す
        const ejssrc = path.join(__dirname, settings.ejs.src); // ejsのルートをセッティングから読み出す
        let relativeRoot = `${path.relative(currentDir, ejssrc)}/`; // 相対パスを割り出す
        relativeRoot = relativeRoot.replace(/[\\|\/\/]/g, "/"); // windows対策
        relativeRoot = relativeRoot.replace(/\/\.\//, ""); // windows対策
        if (
          relativeRoot === "." ||
          relativeRoot === "/" ||
          relativeRoot === ""
        ) {
          relativeRoot = `./`;
        }
        dirHierarchy = path
          .dirname(file.path)
          .replace(`${ejssrc}`, "")
          .split(path.sep);
        rootPathMap = new Map();
        if (`${currentDir}${path.sep}` !== `${ejssrc}`) {
          for (let i = dirHierarchy.length, cnt = 0; i >= 0; i--, cnt++) {
            let targetPath = "";
            let rootPath = "/";
            for (let depth = 1; depth < i; depth++) {
              rootPath += dirHierarchy[depth] + "/";
            }
            switch (cnt) {
              case 0:
                targetPath = "./";
                break;
              default:
                for (let j = cnt; j > 1; j--) {
                  targetPath += "../";
                }
                break;
            }
            rootPathMap.set(targetPath, rootPath);
          }
        } else {
          rootPathMap.set("./", "/");
        }
        // console.log(file.path);
        // console.log(dirHierarchy);
        // console.log(rootPathMap);
        return { $: {path: relativeRoot, pathMap: rootPathMap} }; // file.data.path, file.data.pathMapをつけて次の処理へ
      })
    )
    .pipe(
      $.ejs(pageconfig),
      { root: settings.ejs.src }
    )
    .pipe(
      $.rename(function(file) {
        if (file.basename.match(/.php$/)) {
          file.basename = file.basename.replace(/.php$/, ``); //末尾の.phpを削除
          file.extname = `.php`; //拡張子を.phpに変換
        } else {
          file.extname = settings.ejs.ext;
        }
      })
    )
    .pipe(
      $.prettyHtml({
        indent_size: 2,
        indent_char: " ",
        preserve_newlines: false,
        unformatted: ["code", "pre", "em", "strong", "span", "i", "b", "br"]
      })
    )
    .pipe($.crLfReplace({ changeCode: "CR+LF" }))
    .pipe($.htmlhint({ "doctype-first": false, "spec-char-escape": false }))
    .pipe($.htmlhint.reporter())
    .pipe(gulp.dest(settings.basedir));
  done();
}

/**
 * 画像を圧縮して公開領域へ
 */
function imagemin(done) {
  gulp
    .src(
      [
        `${settings.img.src}**/*.png`,
        `${settings.img.src}**/*.jpg`,
        `${settings.img.src}**/*.jpeg`,
        `${settings.img.src}**/*.gif`,
        `${settings.img.src}**/*.svg`
      ],
      { since: gulp.lastRun(imagemin) }
    )
    .pipe($.plumber({ errorHandler: notifyError() }))
    .pipe(
      through.obj((file, enc, callback) => {
        const syncPath = file.path.replace(
          path.join(__dirname, settings.img.src),
          ""
        ); // srcとdistでの共通パス
        const destPath = `${settings.img.dest}${syncPath}`; // 公開領域側の画像パス
        const srcStat = file.stat; // 開発領域側の画像ファイルステータス
        const destStat = getFileStat(destPath); // 公開領域側の画像ファイルステータス
        // 開発領域側のタイムスタンプが公開領域側のタイムスタンプと同じか古いとき弾く
        if (destStat && srcStat.mtime <= destStat.mtime) {
          file = null;
        }
        callback(null, file);
      })
    )
    .pipe(
      $.imagemin([
        gifsicle(settings.imagemin.gifsicle),
        jpegRecompress(settings.imagemin.jpegRecompress),
        pngquant(settings.imagemin.pngquant),
        svgo(settings.imagemin.svgo)
      ])
    )
    .pipe(gulp.dest(settings.img.dest));
  done();
}

/**
 * SVGスプライトを作成
 */
function sprite(done) {
  gulp
    .src(`${settings.sprite.src}*.svg`, { since: gulp.lastRun(sprite) })
    .pipe(
      $.svgSprite({
        mode: {
          symbol: {
            sprite: `../${settings.sprite.dest}symbol.svg`
          }
        },
        shape: {
          transform: [
            {
              svgo: {
                plugins: [
                  { removeTitle: true }, // titleを削除
                  { removeStyleElement: true }, // styleを削除
                  { removeAttrs: { attrs: "fill" } }, // fillを削除
                  { removeXMLNS: true }, // xmlnを削除
                  { removeDimensions: true } // width/heightを削除
                ]
              }
            }
          ]
        },
        svg: {
          xmlDeclaration: false
        }
      })
    )
    .pipe(gulp.dest(".")); // symbol.svgを生成

  done();
}

/**
 * ファイルステータスを取得
 * 対象が見つからない場合エラーになるので、同期的に処理できるよう切り出した
 * @param {string} path ステータスを取得する対象ファイルのパス
 */
function getFileStat(path) {
  try {
    return fs.statSync(path);
  } catch (err) {
    if (err.code === "ENOENT") {
      return null;
    } else {
      emit("error", err);
      return 1;
    }
  }
}

/**
 * サーバーを立ててブラウザシンクで同期する
 */
function server(done) {
  if (settings.gulp.USE_PHP) {
    // USE_PHP = true のとき
    $.connectPhp.server(
      {
        port: settings.gulp.port,
        base: settings.basedir
      },
      function() {
        browserSync.init({
          proxy: `localhost:${settings.gulp.port}`
        });
      }
    );
  } else {
    // USE_PHP = false のとき
    browserSync.init({
      server: settings.basedir,
      // https: true,
      ghostMode: false,
      reloadDebounce: 2000
    });
  }
  gulp.watch([`${settings.basedir}**/*`]).on("all", browserSync.reload);
  done();
}

/**
 * エラー表示
 */
function notifyError() {
  return $.notify.onError({
    title: "Gulp エラー",
    message: "Error: <%= error.message %>",
    sound: "Funk"
  });
}
