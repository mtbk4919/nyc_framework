const path = require("path"); // 安全にパスを解決する
const fs = require("fs-extra"); // ファイルを操作するrequire(`fs`); // ファイルを操作する
const plugin = require("tailwindcss/plugin"); //Tailwindの自作classを生成するプラグイン

module.exports = {
  target: [
    "ie11",
    {
      objectFit: "default",
      objectPosition: "default",
      position: "default"
    }
  ],
  purge: ["./src/**/*.ejs", "./src/**/*.ts"],
  theme: {
    screens: {
      xs: "375px",
      sm: "640px",
      md: "960px",
      lg: "1240px",
      xl: "1440px"
    },
    fontFamily: {
      gothic: [
        "游ゴシック体",
        "Yu Gothic",
        "YuGothic",
        "メイリオ",
        "Meiryo",
        "ヒラギノ角ゴ Pro",
        "Hiragino Kaku Gothic Pro",
        "MS Pゴシック",
        "MS PGothic",
        "sans-serif"
      ],
      mincho: [
        "游明朝体",
        "Yu Mincho",
        "YuMincho",
        "ヒラギノ明朝 Pro",
        "Hiragino Mincho Pro",
        "MS P明朝",
        "MS PMincho",
        "serif"
      ],
      arial: ["Arial", "sans-serif"]
    },
    borderWidth: {
      default: "1px",
      "0": "0",
      "2": "2px",
      "4": "4px"
    },
    extend: {
      colors: {
        white: "#ffffff",
        "gray-white": "#eeeeee",
        "gray-light": "#cccccc",
        gray: "#999999",
        "gray-dark": "#666666",
        "gray-black": "#333333",
        black: "#000000"
      },
      spacing: {
        "96": "24rem",
        "128": "32rem"
      }
    }
  },
  plugins: [],
  important: true,
  corePlugins: {},
  variants: []
};
