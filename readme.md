# NYC_Framework v1.3.0

## Table Of Contents

1. [このフレームワークでできること](#このフレームワークでできること)
1. [想定ディレクトリ構造](#想定ディレクトリ構造)
1. [標準で使えるライブラリ](#標準で使えるライブラリ)
1. [EJSについて](#EJSについて)
1. [画像の圧縮転送について](#画像の圧縮転送について)
1. [SVGスプライトの生成について](#SVGスプライトの生成について)
1. [開発環境について](#開発環境について)
1. [主な実行コマンド](#主な実行コマンド)
1. [その他の実行コマンド](#その他の実行コマンド)
1. [settings.jsonについて](#settings.jsonについて)
1. [webpackによるモジュールの分離について補足説明](#webpackによるモジュールの分離について補足説明)
1. [CSS設計方針](#CSS設計方針)

## このフレームワークでできること

- 環境のインストール時に空のディレクトリ構造を自動作成します
- ejsをコンパイルできます（gulp-ejs）
- ejs内でサイトルートへの相対パス(path)を使用できます（gulp-data）
- ejsはpage-config.jsonの内容を使用できます。（gulp-ejs）
- 出力されたhtmlの文法チェックを行います（html-hint）
- 出力されたhtmlの整形を行います（gulp-pretty-html）
- scssをコンパイルできます（webpack経由のnode-sass)
- scssの文法チェックと自動修正ができます（stylelint）
- scssのソースマップを出力します（webpack ※デベロップメントモード）
- cssにベンダープレフィックスを自動付与します（post-css,autoprefixer）
- cssの圧縮・メディアクエリの結合ができます（cssnano, css-mqpacker ※プロダクションモード）
- javascriptをES6で書いてもpolyfillするので動きます（webpack）
- javascriptをモジュール化して開発し、共通部分バンドルや特定モジュールのセパレートができます（webpack）
- javascriptの依存関係を自動解決するので、非同期読み込みできます（webpack）
- javascriptの文法チェックと自動修正ができます（eslint）
- javascriptの圧縮ができます（UglifyJS ※プロダクションモード）
- javascriptのソースマップを出力します（webpack ※デベロップメントモード）
- png,jpg,gif,svgの圧縮ができます（gulp-imagemin,gifsicle,mozjpeg,pngquant,svgo）
- SVGスプライトの生成ができます（gulp-svg-sprite）
- ローカルサーバーを立ち上げて動作を確認できます（browser-sync）
- 組み込みのPHPサーバーも使用可能です（PHPのパスが通っている必要あり ※USE_PHPがtrue）

---

## 標準で使えるライブラリ

- jQuery: どこでも$で呼び出せます
- lodash: 必要な関数を直接指定でimportして使用します
- swiper: 必要なページでvendorから読み出して使います
- svgxuse: SVGスプライトを使用するため、main.jsでimportしておきます

その他のライブラリもNPM,CDNを問わず簡単に追加することができます。

---

## EJSについて

EJSはHTMLのテンプレート言語です。

EJS内部では次の変数が使用できます

- path: 処理中のEJSから公開領域ルート(デフォルト:dist/)への相対パス
- src/page-config.jsonに記載されたデータ

また、ファイル名が_(アンダースコア)で始まるEJSはhtml/phpを生成しません。
インクルード用データとして使用してください。

### デモページのEJSに記述した機能

- page-config.jsonの情報を元にパンくずリストを自動生成します
- page-config.jsonの情報を元にパンくずリストの構造化データ(JSON-LD)を自動生成します
- page-config.jsonの情報を元にOGPを自動生成します
- page-config.jsonの情報を元にcanonical, robotsなども自動生成します

---

## 画像の圧縮転送について

npm run startした状態でsrc/img/optimize以下にpng,jpg,gif,svgを配置すると、配置された画像を圧縮して公開領域へ転送します。
圧縮転送の際にはディレクトリ構造も転送されるので、必要なディレクトリを予め作成しておくと便利です。

圧縮したくない場合は、公開領域へ直接配置することで圧縮を回避できます。

ちなみに、公開領域のassets/img以下にディレクトリが追加されると、開発領域のsrc/img/optimize以下にもディレクトリが追加されます。

なお、デフォルト設定では公開領域の画像のみをgitで差分管理します。

本フレームワークの特徴として、開発領域側の画像データのタイムスタンプが公開領域側の画像データのタイムスタンプよりも古い場合、圧縮工程をスキップする点が挙げられます。これにより、無駄な圧縮作業をなくすとともに、ビルド時の画像圧縮にかかる負担・時間を開発段階に分散することができました。

---

## SVGスプライトの生成について

SVGスプライトの特徴はCSSでfill:{{color}}を指定することでパスの色を自由に変えられることです。

npm run startした状態でsrc/img/spritesにsvgを配置すると、dist/assets/img/common/spritesにSVGスプライトを生成します。
SVGスプライトはHTMLに以下のコードを記述することで使用できます。

```html
<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<%= path %>assets/img/common/sprites/sprite.symbol.svg#{{ファイル名}}"></use></svg>
```

また、src/scss/utility/_sprite.scssにはCSSスプライトとして使用できるクラスが記載されています。
ぜひ活用してください。

---

## 開発環境について

開発を始める前に環境をインストールする必要があります。
ターミナル（コマンドラインツール）を開いてカレントディレクトリをプロジェクトディレクトリ（package.jsonと同じ階層）に移動します。
移動方法はcd {{path}}です。
移動したら、npm install（もしくは yarn install）でインストールが始まります。

あとで詳しく説明しますが、
webpackやgulpで使用する設定等はsettings.jsonに集めてあります。
lintの設定については.eslintrc .stylelintrcで個別に設定して下さい。
npmでも動くと思いますが、yarn推奨です。

---

## 主な実行コマンド

### npm run start (もしくは yarn start)

開発を始める際に使用してください。
ブラウザシンクが立ち上がり、更新を検出してホットリロードします。
ソースコードの圧縮は行われません、ソースマップは出力されます。
終了する時はCtrl+C（mac: Control + C）です。

### npm run start-pro (もしくは yarn start-pro)

本番公開する前に使います。
ブラウザシンクが立ち上がり、更新を検出してホットリロードします。
ソースコードの圧縮が行われます。ソースマップは出力されません。
公開前の稼働チェックに使えますが、処理にとても時間がかかります。
終了する時はCtrl+C（mac: Control + C）です。

### npm run build (もしくは yarn build)

本番公開する前に使います。ブラウザシンクは立ち上がりません。
画像の圧縮転送が行われます。スプライト画像の生成が行われます。
ソースコードの圧縮が行われます。ソースマップは出力されません。
ソースコードの圧縮が入るため、開発時とは別に稼働チェックが必要です。

### npm run build-dev (もしくは yarn build-dev)

試しにビルドする際に使います。ブラウザシンクは立ち上がりません。
画像の圧縮転送が行われます。スプライト画像の生成が行われます。
ソースコードの圧縮は行われません、ソースマップは出力されます。

---

## その他の実行コマンド

### npx gulp ejs

gulp buildに含まれるタスクのうち、EJSのコンパイルのみ実行します

### npx gulp imagemin

gulp buildに含まれるタスクのうち、画像の圧縮転送のみを実行します

### npx gulp sprite

gulp buildに含まれるタスクのうち、スプライト画像の生成のみ実行します

---

## settings.jsonについて

webpackとgulpで使用する個別設定はsettings.jsonにまとめてあります。

- dir: 自動作成するディレクトリ。
- basedir: 公開領域のルート、サーバールート。
- *.src: 開発領域のルート、コンパイル元に使用します。
- *.dest: 公開領域のルート、コンパイル先に使用します。
- *.vendor: npmでもcdnでもないライブラリ等を置くディレクトリ、公開領域に転送されます。
- ejs.ext: ejsを変換した後の拡張子を指定します。.が必要です。
- webpack.SEPARATE_VENDOR: trueにすると、node_modulesのモジュールをvendor.jsに分離します。
- webpack.SEPARATE_MODULES: trueにすると、共通モジュールをmodules.jsに分離します。
- webpack.entry: キーに出力先（拡張子なし）、値に入力元を指定します。個別ページで読み込みたいスクリプトがある場合などに追記します。
- webpack.resolve: webpack内で自動解決する拡張子です。
- webpack.externals: vendorディレクトリやCDNから読み込む外部依存スクリプトを記載します。eslintに怒られたらeslintrcのglobalsに追加してください。
- webpack.vendor: webpack.SEPARATE_VENDORがtrueのとき、指定されたモジュール名を手がかりにvendor.jsから{{モジュール名}}.jsに分離します。
- webpack.provide: 各スクリプトでimportしなくてもモジュールを自動的に使えるようになる。
- imagemin: 画像圧縮に使う設定、詳細はプラグイン名で検索してください。
- gulp.USE_PHP: trueにするとPHPサーバー経由で起動します。windowsで実行する場合は事前にPHPのパスを通しておく必要があります。

---

## webpackによるモジュールの分離について補足説明

### 1. webpack.SEPARATE_VENDOR=false, webpack.SEPARATE_MODULES=false のとき

一切の分離を行いません。
共通モジュールが使われているエントリーポイント毎に重複してコンパイルされます。
あまりライブラリを使わないような場合はこれで十分でしょう。

【出力】

- main.js

### 2. webpack.SEPARATE_VENDOR=true, webpack.SEPARATE_MODULES=false のとき

node_modules内のモジュールはvendor.jsに分離されます。
複数のエントリーポイントから読み込まれている共通モジュールもvendor.jsに分離されます。
さらに、webpack.vendorで指定したモジュールはvendor.jsから分離されます。

【出力】

- main.js
- vendor.js
- jquery.js

### 3. webpack.SEPARATE_VENDOR=false, webpack.SEPARATE_MODULES=true のとき

node_modules内のモジュールはmodules.jsに分離されます。
複数のエントリーポイントから読み込まれている共通モジュールもmodules.jsに分離されます。
ただし、webpack.vendorで指定しても個別に分離されません。
２と比較してメリットはありません。

【出力】

- main.js
- modules.js

### 4. webpack.SEPARATE_VENDOR=true, webpack.SEPARATE_MODULES=true のとき

node_modules内のモジュールはvendor.jsに分離されます。
さらに、webpack.vendorで指定したモジュールはvendor.jsから分離されます。
しかも、複数のエントリーポイントから読み込まれている共通モジュールはmodules.jsに分離されます。

【出力】

- main.js
- vendor.js
- jquery.js
- modules.js

状況に応じて 1、２ もしくは 4 がおすすめです。

１はエントリーポイントが１つでライブラリが大きくない時に適切です。
２はエントリーポイントが１つで複数のライブラリを使う場合に適切です。
３は２に近いですがwebpack.vendorで指定したモジュールを分離しません。
４は複数のエントリーポイントがある場合に適切です。

なお、vendor.js, jquery.js, modules.jsなどをhead内で非同期読み込みにするのがおすすめです。
webpackが依存関係を解決してくれているので、main.jsは読み込みを待って実行してくれます。（要検証）

---

## CSS設計方針

MindBEMding と FLOU をベースに独自拡張する。

### ハッピーなCSSコーディングのための心得

1. position(relativeを除く), margin, float, width, heightは外へのレイアウト
1. display, padding, borderは内へのレイアウト
1. 打ち消すよりもクラスを増やそう
1. メディアクエリはモバイルファーストでbreak-up
1. メディアクエリはBlockごとにまとめる

### CSS命名規則 - BEM

#### 基本解説

https://qiita.com/Takuan_Oishii/items/0f0d2c5dc33a9b2d9cb1

#### ローカルルール（強制ではないが推奨）

1. FLOU（後述）に従ってBlockに接頭辞をつける
1. Blockにはmarginやposition:absoluteなど外へのレイアウトに関するスタイリングを行わない（ただし、FLOUにおけるLayout層を除く）
1. Modifireをつけるとき、BlockとElementは省略してもよく、接頭辞 `-` を使用する。
1. 一時的な状態を示すクラスは modifireではなくis-hogeを使う
1. BlockからElementをネストするのはOK、BlockやElementの途中でネストするのはNG

#### サンプルコード１

```html
<!-- html -->
<nav class="l-nav">
  <ul class="l-nav__list list">
    <li class="list__item -current">hoge</li>
    <li class="list__item is-active">hoge</li>
  </ul>
</nav>
```

```scss
// layout/_nav.scss
.l-nav {
  width: 100%;

  &__list {
    width: 50%;
    margin: auto;
  }
}
```

```scss
// object/_list.scss
.list {
  display: block;
  padding-left: 0;
  list-style-type: none;

  &__item {
    display: block;
    border-left: 4px solid #ccc;
    padding-left: 4px;

    &.-current {
      border-color: #999;
    }
    &.is-active {
      background-color: #eee
    }
  }
}
```

### CSSのモジュール化

ドメインレイヤー x コンサーンレイヤー の考え方で整理する。
その他、global

#### 領域レイヤー（第一階層：領域分類)

[CROCCS](https://qiita.com/croco_works/items/28b5033448e54d2946c7)

領域の分離を担当するレイヤー。

- base :サイト全体を横断する
- frame :ヘッダー・フッター・サイドバーなど枠組み
- contents :メインコンテンツ
- pages :個別ページ（接頭辞 `p-`）

#### base

サイト全体を横断する、`function`や`utility`のほか`reset`なども含まれる。

#### frame

ヘッダー・フッター・サイドバーなどメインコンテンツ以外の枠組み

#### contents

メインコンテンツ

#### pages

クラス名には接頭辞 `p-` をつける。

個別ページごとに適用する。

※関心レイヤーと併せて使用する場合、layoutは `pl-`, objectは `p-` とする。

### 関心レイヤー（第二階層: 関心分類）

関心の分離を担当するレイヤー。
上述CROCCSにおけるデータ分類をもうすこしシンプルにしたもの。

[FROU](https://qiita.com/croco_works/items/28b5033448e54d2946c7)

- function :機能を提供する
- layout :コンポーネントの配置を制御する（接頭辞 `l-`）
- object :コンポーネントの見た目・構造を制御する
- utility :汎用クラスを提供する（接頭辞 `_`)

#### functions

コードを生成しないscss上の機能を提供する。

#### layout

クラス名には接頭辞 `l-` をつける

margin, width, flex, float, position:absolute など**レイアウトに関するスタイリングを専門に行う。**
逆に、colorやfont-sizeなどの指定はここで行うべきではない。

#### object

サイト内で繰り返し使いそうな要素・パーツで使用する。
**結局1回しか使わなくてもいいので遠慮なく増やす。**

ただし、marginやposition:absoluteなど外へのレイアウトに関するスタイリングを行わないように注意する。
内へのレイアウトについてはobject内で行ってもよい。

#### utility

クラス名には接頭辞 `_` をつける。

要素間の余白の調整や色の細かい変更などで使う便利なやつ。
いわゆるヘルパークラス、汎用クラス。**修正が大変になることがあるので多用しすぎに注意**

- _margin.scss
- _sprite.scss
