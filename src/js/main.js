/* Pluguins
==================== */
// IE11用のsvgスプライトポリフィル
import(/* webpackChunkName: "common" */ "svgxuse");
// IE11用のpicturefillポリフィル
import(/* webpackChunkName: "common" */ "picturefill");
// IE11用のobject-fitポリフィル
import(/* webpackChunkName: "common" */ "object-fit-images");
// 画像の遅延読み込み
import(/* webpackChunkName: "common" */ "lazysizes");
// アウトライン制御
import(/* webpackChunkName: "common" */ "what-input");

// import $ from 'jquery'; // jQuery:$はimportしなくてもどこでも使えます
// import _ from 'lodash'; // lodashは使用する関数を直接指定してimportしてください
// lodashの読み込み参考: https://s8a.jp/javascript-lodash-reduce-file-size#lodash%E3%81%AE%E5%88%A9%E7%94%A8%E3%81%99%E3%82%8B%E9%96%A2%E6%95%B0%E3%82%92%E7%9B%B4%E6%8E%A5%E4%BD%BF%E7%94%A8%E3%81%99%E3%82%8B%E7%9B%B4%E6%8E%A5%E6%8C%87%E5%AE%9A

/* Libraries
==================== */
import _userAgent from "./library/_userAgent";
// import { AriaModal,  AriaTab } from "../library/_aria";
// import { getCookie, setCookie, clearCookie } from "./library/_cookie";
// import { ScrollEvent } from "./library/_scrollEvent";

/* Modules
==================== */
import _gnav from "./modules/_gnav";

/* Script
==================== */
// コンソールログはプロダクションモードでは取り除かれるので、自由に使ってもらって構いません。

window.addEventListener("DOMContentLoaded", evt => {
  /* User Agent
  ==================== */
  _userAgent();
});
