// import { ScrollEvent } from "./modules/_scrollEvent";

/**
 * スクロールイベントを管理する
 */
export class ScrollEvent {
  constructor() {
    this.pre = [];
    this.up = [];
    this.down = [];
    this.post = [];
    this.prop = {
      nowY: window.pageYOffset,
      nowX: window.pageXOffset,
      oldY: window.pageYOffset,
      oldX: window.pageXOffset
    };
    this.throttleTimer = null;
  }

  /**
   * 関数をキューイングする
   * @param {Function} func キューイングする関数
   * @param {String} when default: pre; pre, up, down, postから選択
   * @param {String} method default: push; push, unsfhitから選択
   */
  enqueue(func, when = "pre", method = "push") {
    let targetQueue = null;

    switch (when) {
      case "pre": {
        targetQueue = this[when];
        break;
      }
      case "up": {
        targetQueue = this[when];
        break;
      }
      case "down": {
        targetQueue = this[when];
        break;
      }
      case "post": {
        targetQueue = this[when];
        break;
      }
      default: {
        const error = new Error(
          'You can only set 2nd argument "pre", "up", "down" or "post".'
        );
        throw error;
      }
    }

    switch (method) {
      case "push": {
        targetQueue.push(func);
        break;
      }
      case "unshift": {
        targetQueue.unshift(func);
        break;
      }
      default: {
        const error = new Error(
          'You can only set 3rd argument "push" or "unshift".'
        );
        throw error;
      }
    }
  }

  update() {
    this.prop.oldY = window.pageYOffset;
    this.prop.oldX = window.pageXOffset;
  }

  activate() {
    window.addEventListener(
      "scroll",
      evt => {
        if (this.throttleTimer) {
          return;
        } else {
          // throttle
          this.throttleTimer = window.setTimeout(() => {
            // execute pre
            this.pre.reduce((acc, cur) => {
              return cur(acc);
            }, evt);

            window.clearTimeout(this.throttleTimer);
            this.throttleTimer = null;
          }, 100);

          // main thread
          this.prop.nowY = window.pageYOffset;
          this.prop.nowX = window.pageXOffset;

          // execute up or down
          if (this.prop.oldY > this.prop.nowY) {
            document.body.classList.add("js-scroll-up");
            document.body.classList.remove("js-scroll-down");

            this.up.reduce((acc, cur) => {
              return cur(acc);
            }, evt);
          } else if (this.prop.oldY < this.prop.nowY) {
            document.body.classList.remove("js-scroll-up");
            document.body.classList.add("js-scroll-down");

            this.down.reduce((acc, cur) => {
              return cur(acc);
            }, evt);
          }

          // execute post
          this.post.reduce((acc, cur) => {
            return cur(acc);
          }, evt);
        }
        this.update();
      },
      false
    );
  }
}
