/*
 * Modules
======================================== */
const webpack = require("webpack"); // webpack
const path = require("path"); // 安全にパスを解決する
const glob = require("glob"); // globを使用する
const settings = require(path.join(__dirname, "settings.json")); // 初期設定はsettings.jsonにまとめる
const StyleLintPlugin = require("stylelint-webpack-plugin"); // stylelintを使う
const MiniCssExtractPlugin = require("mini-css-extract-plugin"); // CSSを取り出す
const UglifyJsPlugin = require("uglifyjs-webpack-plugin"); // JSを圧縮する
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries"); // 余分なJSを出さない
const PurifyCSSPlugin = require("purifycss-webpack"); // 使用されていないルールを削除する

/**
 * @param  {string} env
 * @param  {object} argv
 * argv.modeにwebpackを実行したモードが格納されている。
 */
module.exports = (env, argv) => {
  // ソースマップ（USE_SOURCEMAP）はデフォルトで無効（developmentモードで有効）
  // コード圧縮(USE_MINIFY)はデフォルトで有効（developmentモードで無効）
  // コードの自動修復はデフォルトで無効 --fix をつけて実行すると有効
  // 共通ファイルの分割（SEPARATE_VENDOR)はデフォルトで有効
  // ライブラリの個別分割（SEPARATE_LIBRARIES)はデフォルトで有効
  const MODE = argv.mode;
  const IS_DEVELOPMENT = MODE === "development";
  const USE_SOURCEMAP =
    settings.webpack.USE_SOURCEMAP === "auto"
      ? IS_DEVELOPMENT
      : settings.webpack.USE_SOURCEMAP;
  const USE_MINIFY =
    settings.webpack.USE_MINIFY === "auto"
      ? !IS_DEVELOPMENT
      : settings.webpack.USE_MINIFY;
  const USE_FIX = argv.fix ? true : false;
  const SEPARATE_VENDOR = settings.webpack.SEPARATE_VENDOR;
  const SEPARATE_MODULES = settings.webpack.SEPARATE_MODULES;
  const ENTRIES = settings.webpack.entry;
  const DOTENV = IS_DEVELOPMENT
    ? require("dotenv").config({
        path: path.resolve(process.cwd(), ".env.development")
      })
    : require("dotenv").config({ path: path.resolve(process.cwd(), ".env") });
  if (!DOTENV) throw new Error("There is no .env file or .env.development");

  if (settings.css.USE_AUTO_ENTRY) {
    Object.assign(
      ENTRIES,
      glob
        .sync("**/*.scss", {
          ignore: ["**/_*.scss", "vendor/**/*"],
          cwd: settings.css.src
        })
        .reduce((acc, cur) => {
          const srcPath = path
            .resolve(settings.css.src, cur)
            .replace(/\.scss$/, "");
          const destPath = path
            .resolve(settings.css.dest, cur)
            .replace(/\.scss$/, "");
          acc[path.relative(__dirname, destPath)] = srcPath;
          return acc;
        }, {})
    );
  }
  if (settings.js.USE_AUTO_ENTRY) {
    Object.assign(
      ENTRIES,
      glob
        .sync("**/*.js", {
          ignore: ["**/_*.js", "vendor/**/*"],
          cwd: settings.js.src
        })
        .reduce((acc, cur) => {
          const srcPath = path
            .resolve(settings.js.src, cur)
            .replace(/\.js$/, "");
          const destPath = path
            .resolve(settings.js.dest, cur)
            .replace(/\.js$/, "");
          acc[path.relative(__dirname, destPath)] = srcPath;
          return acc;
        }, {})
    );
  }

  let PLUGINS = [
    new webpack.ProgressPlugin(),
    new FixStyleOnlyEntriesPlugin(),
    new MiniCssExtractPlugin(),
    new StyleLintPlugin({ fix: USE_FIX }),
    new webpack.ProvidePlugin(settings.webpack.provide)
  ];
  if (USE_MINIFY) {
    PLUGINS.push(
      new PurifyCSSPlugin({
        paths: glob.sync(
          path.join(__dirname, `${settings.basedir}**/*.{html,xhtml,php}`)
        ),
        purifyOptions: {
          whitelist: ["*js-*", "*is-*", "*modal*", ".invisible", "*animate*"]
        },
        minimize: true
      })
    );
  }

  console.log("# === Webpack processing! === #");
  console.log("■ 実行モード: " + MODE);
  console.log("■ ソースマップ: " + USE_SOURCEMAP);
  console.log("■ コード圧縮: " + USE_MINIFY);
  console.log("■ コード修正: " + USE_FIX);
  console.log(
    "■ エントリーポイント: " + JSON.stringify(settings.webpack.entry)
  );
  console.log("■ ベンダー分離: " + SEPARATE_VENDOR);
  console.log("■ ベンダー個別分離指定: " + settings.webpack.vendor);
  console.log("■ 共通モジュール分離: " + SEPARATE_MODULES);
  console.log("■ 外部依存設定: " + settings.webpack.externals);
  console.log("■ 自動インポート: " + JSON.stringify(settings.webpack.provide));
  console.log("# === Please wait. === #");

  return [
    {
      // モードの設定、v4系以降はmodeを指定しないと、webpack実行時に警告が出る
      mode: MODE,

      // 入力
      entry: ENTRIES,

      // 出力
      output: {
        filename: "[name].js",
        chunkFilename: path.join(settings.js.dest, "[name].bundle.js"),
        path: path.join(__dirname, settings.basedir),
        publicPath: process.env.PUBLIC_PATH
      },

      // プラグイン
      plugins: PLUGINS,

      resolve: {
        extensions: settings.webpack.resolve.extensions,
        alias: {
          NODE_ENV: process.env.NODE_ENV,
          PUBLIC_PATH: process.env.PUBLIC_PATH
        }
      },

      // 外部依存の設定
      externals: settings.webpack.externals,

      // ソースマップの切り替え
      devtool: USE_SOURCEMAP ? "source-map" : "none",

      module: {
        rules: [
          {
            test: /\.(js|jsx)$/,
            exclude: /node_modules\/(?!(dom7|swiper)\/).*/,
            loader: "babel-loader",

            options: {
              plugins: ["syntax-dynamic-import"],

              presets: [
                [
                  "@babel/preset-env",
                  {
                    useBuiltIns: "usage",
                    corejs: "core-js@3",
                    modules: false
                  }
                ]
              ]
            }
          },
          {
            enforce: "pre",
            test: /\.(js|jsx)$/,
            exclude: /node_modules\/(?!(dom7|ssr-window|swiper)\/).*/,
            use: [
              {
                loader: "eslint-loader",
                options: {
                  globals: [...settings.webpack.externals],
                  fix: USE_FIX
                }
              },
              "import-glob-loader"
            ]
          },
          {
            test: /\.(sass|scss|css)$/,
            exclude: /node_modules/,
            use: [
              MiniCssExtractPlugin.loader,
              "css-loader?{url:false,sourceMap:true}",
              {
                loader: "postcss-loader",
                options: {
                  sourceMap: true,
                  plugins: [
                    require("postcss-object-fit-images"),
                    require("tailwindcss"),
                    require("autoprefixer")
                  ]
                }
              },
              {
                loader: "sass-loader",
                options: {
                  sourceMap: true,
                  data: "@import 'global-imports.scss';",
                  includePaths: [path.resolve(__dirname, settings.css.src)],
                  outputStyle: "expanded"
                }
              }
            ]
          },
          {
            enforce: "pre",
            test: /.(sass|scss|css)$/,
            exclude: /node_modules/,
            use: ["import-glob-loader"]
          },
          {
            test: /\.css$/,
            use: [
              "style-loader",
              {
                loader: "css-loader",
                options: {
                  url: false
                }
              }
            ]
          }
        ]
      },

      optimization: {
        // 圧縮
        minimize: USE_MINIFY,
        minimizer: [
          new UglifyJsPlugin({
            uglifyOptions: {
              ecma: 6,
              output: {
                comments: false,
                ascii_only: true
              },
              compress: {
                drop_console: true,
                comparisons: false
              }
            }
          })
        ]
      },

      devServer: {
        open: true
      }
    }
  ];
};
